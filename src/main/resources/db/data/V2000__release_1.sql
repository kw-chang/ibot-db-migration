-- Add system required roles
INSERT INTO `roles` (`id`,`name`) VALUES (1,'SuperAdministrator'),(2,'Admin');

-- Add client required roles
INSERT INTO `roles` (`id`,`name`) VALUES (3,'Sales'),(4,'Technical Support');

-- Add app profile
INSERT INTO `app_profile` (id,whatsapp_account_id, whatsapp_account_name, business_name, template_namespace, facebook_business_manager_id, whatsapp_business_account_id, phone_number, status) VALUES 
	(1,'8dfa8572-7764-4526-97f7-af6df275dce2','Service4U', 'Service4U Sdn. Bhd.', 'a4daed7b_b9ca_443b_9671_32b8adc05c5f', '173849514179855', '648444156057217', '60102152762', 'ACTIVE');

-- Add super administrator for system
INSERT INTO `auth_users` (`id`,`activated`,`created_date`,`email`,`password`,`phone_number`,`updated_date`,`last_name`,`first_name`,`app_profile_id`) VALUES 
	(1,1,now(),'admin@ibot.com','$2a$10$iwzE78lbiHJxPQqoopZR1.a44DYXEmXcEGwlcnbYovvlFXrHk5Aby','60168153603',now(),'sysadmin','service4u',null);

-- Add admin user for app
INSERT INTO `auth_users` (`id`,`activated`,`created_date`,`email`,`password`,`phone_number`,`updated_date`,`last_name`,`first_name`,`app_profile_id`) VALUES 
	(2,1,now(),'admin@service4u.com','$2a$10$iwzE78lbiHJxPQqoopZR1.a44DYXEmXcEGwlcnbYovvlFXrHk5Aby','60168153603',now(),'admin','service4u',1),
    (3,1,now(),'sales@service4u.com','$2a$10$VZJYE8bDoXWvgVZADYWKeulLyDNVA0nPThbwfUZRr2BBvkIdWkkVS','601682916119',now(),'sales','service4u',1),
	(4,1,now(),'technical@service4u.com','$2a$10$VZJYE8bDoXWvgVZADYWKeulLyDNVA0nPThbwfUZRr2BBvkIdWkkVS','601682916119',now(),'technical','service4u',1);

-- Assign user to role
INSERT INTO `user_role` (user_id, role_id) VALUES (1,1);
INSERT INTO `user_role` (user_id, role_id) VALUES (2,2);

-- Create sample topics
INSERT INTO `topics` (`id`,`assigned_template_id`,`chat_identifier`,`description`,`name`,`template_message`) VALUES
	(1,'new_message_for_support','1','Sales support','Sales','Dear {{1}}, you have 1 new message from {{2}}. Kindly login to the system and assist the customer. Thank you. [THIS IS AN AUTOMATED MESSAGE - PLEASE DO NOT REPLY DIRECTLY TO THIS MESSAGE]'),
	(2,'new_message_for_support','2','Technical Support','Technical','Dear {{1}}, you have 1 new message from {{2}}. Kindly login to the system and assist the customer. Thank you. [THIS IS AN AUTOMATED MESSAGE - PLEASE DO NOT REPLY DIRECTLY TO THIS MESSAGE]');

-- Assign roles to topic
INSERT INTO `topic_role` (topic_id, role_id) VALUES (1,3);
INSERT INTO `topic_role` (topic_id, role_id) VALUES (1,4);


-- Add configurations
INSERT INTO `configuration_settings` (`id`,`created_on`,`setting_key`,`config_type`,`multi_value`) VALUES 
	(1,now(),'AGENTS_LIMIT','SYSTEM',0),
	(2,now(),'DEFAULT_INITIAL_RESPONSE','CLIENT',0),
    (3,now(),'THANK_YOU_MESSAGE','CLIENT',0),
    (4,now(),'UNREGCONIZED_MESSAGE','CLIENT',0);

INSERT INTO `configuration_setting_values` (`id`,`created_on`,`setting_value`,`configuration_setting_id`) VALUES 
	(1,now(),'5',1),
    (2,now(),'Thank you for contacting Service4u, your preferred services provider.\\n',2),
    (3,now(),'Thank you for your reply. You have selected service option %s. Our customer representative will attend to you shortly. Have a nice day!',3),
    (4,now(),'Sorry, we do not understand your request.',4);







