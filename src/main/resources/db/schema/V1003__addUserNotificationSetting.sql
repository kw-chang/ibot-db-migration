create table user_notification_settings (
	id bigint not null auto_increment, 
    user_id bigint,
    whatsapp_enabled bit(1) not null,
    email_enabled bit(1) not null,
    created_on datetime(6),
    updated_on datetime(6),
    updated_by bigint,
    primary key (id)
);

alter table user_notification_settings add constraint fk_notification_setting_user_id foreign key (user_id) references auth_users (id);


