create table tasks (
	id bigint not null auto_increment, 
    conversation_id bigint,
    ended_on datetime(6),
    status varchar(20),
    created_on datetime(6),
    updated_on datetime(6),
    updated_by bigint,
    primary key (id)
);

alter table tasks add constraint fk_task_conversation_id foreign key (conversation_id) references conversations (id);


