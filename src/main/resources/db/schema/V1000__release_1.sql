create table ibot.app_profile (
	id bigint not null auto_increment, 
    business_name varchar(100), 
    facebook_business_manager_id varchar(50), 
    phone_number varchar(25), status varchar(50), 
    template_namespace varchar(50), 
    whatsapp_account_id varchar(50), 
    whatsapp_account_name varchar(100), 
    whatsapp_business_account_id varchar(50), 
    primary key (id), 
    unique key unique_business_name (business_name),
    unique key unique_whatsapp_account_id (whatsapp_account_id), 
    index idx_status (status)) engine=InnoDB;

create table ibot.auth_users (
	id bigint not null auto_increment, 
    activated bit not null, 
    created_date datetime(6), 
    email varchar(100), 
    password varchar(200), 
    phone_number varchar(25), 
    updated_date datetime(6), 
    last_name varchar(100), 
    first_name varchar(100),
    app_profile_id bigint, 
    primary key (id),
    unique key unique_email(email)) engine=InnoDB;

create table ibot.auth_token (
	id bigint not null auto_increment, 
    created_by bigint, 
    created_on datetime(6), 
    expired_on datetime(6), 
    status varchar(50), 
    token varchar(255), 
    updated_on datetime(6), 
    version bigint,
    primary key (id),
    index idx_version(version),
    index idx_token(token),
    index idx_token_status(token, status),
    index idx_created_by_status(created_by, status)) engine=InnoDB;

create table ibot.roles (
	id bigint not null auto_increment, 
    name varchar(50), 
    primary key (id)) engine=InnoDB;
    
create table ibot.topics (
	id bigint not null auto_increment, 
    assigned_template_id varchar(100), 
    chat_identifier varchar(50), 
    description varchar(300), 
    name varchar(100), 
    name_space varchar(250),
    template_message varchar(5000), 
    primary key (id), 
    unique key unique_template(id)) engine=InnoDB;

create table ibot.user_role (
	user_id bigint not null, 
	role_id bigint not null) engine=InnoDB;
    
create table ibot.topic_role (
	topic_id bigint not null, 
    role_id bigint not null) engine=InnoDB;

create table ibot.conversations (
	id bigint not null auto_increment, 
	created_on datetime(6), 
	from_number varchar(25), 
	waba_number varchar(25),
    last_sent_message_on datetime(6), 
    last_sent_by varchar(25), 
	primary key (id)) engine=InnoDB;

create table ibot.chats (
	id bigint not null auto_increment, 
    content_type varchar(30), 
    created_on datetime(6), 
    message varchar(8000), 
    read_at datetime(6), 
    sent_by varchar(50), 
    status varchar(50), 
    updated_on datetime(6), 
    url varchar(2000), 
    wa_msg_id varchar(100), 
    conversation_id bigint, 
    primary key (id), 
    index idx_status (status)) engine=InnoDB;
    
create table ibot.pending_option_replies (
	id bigint not null auto_increment, 
    from_number varchar(25), 
    waba_number varchar(25), 
    primary key (id), 
    unique key unique_phone_number (from_number, waba_number)) engine=InnoDB;
    
create table ibot.configuration_settings (
	id bigint not null auto_increment, 
    created_on datetime(6), 
    setting_key varchar(100), 
    config_type varchar(20),
    multi_value bit, primary key (id)) engine=InnoDB;
    
create table ibot.configuration_setting_values (
	id bigint not null auto_increment, 
    created_on datetime(6), 
    setting_value varchar(1000), 
    configuration_setting_id bigint, 
    primary key (id)) engine=InnoDB;
    
alter table ibot.auth_users add constraint fk_app_profile_id foreign key (app_profile_id) references app_profile (id);

alter table ibot.chats add constraint fk_conversation_id foreign key (conversation_id) references conversations (id);

alter table ibot.configuration_setting_values add constraint fk_conf_setting_id foreign key (configuration_setting_id) references configuration_settings (id);

alter table ibot.topic_role add constraint fk_topic_role_id foreign key (role_id) references roles (id);

alter table ibot.topic_role add constraint fk_topic_id foreign key (topic_id) references topics (id);

alter table ibot.user_role add constraint fk_user_role_id foreign key (role_id) references roles (id);

alter table ibot.user_role add constraint fk_user_id foreign key (user_id) references auth_users (id);