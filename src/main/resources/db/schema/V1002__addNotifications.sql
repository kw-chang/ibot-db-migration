create table notifications (id bigint not null auto_increment, created_on datetime(6), message varchar(255), read_at datetime(6), read_by varchar(255), topic_id bigint, primary key (id));

alter table notifications add constraint fk_notification_topic_id foreign key (topic_id) references topics (id);