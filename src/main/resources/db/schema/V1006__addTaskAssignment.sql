alter table tasks add column assignee bigint default null;
alter table tasks add column close_at datetime(6) default null;
alter table tasks add column close_by bigint default null;


create table task_assignments (
	id bigint not null auto_increment, 
    task_id bigint,
    assigned_by bigint,
    assigned_to bigint,
    assigned_on datetime(6),
    primary key (id)
);

alter table task_assignments add constraint fk_task_task_assignment_id foreign key (task_id) references tasks (id);
