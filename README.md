# ibot-db-migration
The purpose of this project is to manage database migration and version control using flyway with spring boot.

### Important notes ###
1. Add new import data sql into data folder with incremental version.(eg; data/V2001__release_2.sql). Data sql version starts with 2000.
2. Add new create/alter schema sql into schema folder with incremental version. (eg; schema/V1001_release_2.sql). Schema sql version starts with 1000.

Usage:
1. mvn install
2. Go to ibot-db-migration\target, copy ibot-db-migration-0.0.1-SNAPSHOT.jar
3. Upload ibot-db-migration-0.0.1-SNAPSHOT.jar into target server(eg; aws ec2, gcp, azure). Skip this step for local development.
4. Execute java -jar ibot-db-migration-0.0.1-SNAPSHOT.jar
5. Verify changes in database.
